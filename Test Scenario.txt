Test Scenario:A (TS:A)
===============

## ID:1 Filtering by content category

### Description

Check the possibility to find _prowly_ by using filter via google.com

### Pre-Conditions

User needs to go on the [google.com](https://google.com) and click the search textarea

### Expected results

 Google should work and user should have possibility to write something in the search textarea

## Test case ID: TC:A-0001 
Scenario ID: 1   
Test case: All  
Test case description:  testing images filter
Test step: 


1. *I'm writing 'Prowly' and clicking the submit button (magnifier icon)*  
Expected Result: The result should be prowly and prowly related websites   
Current status: ✔️ Passed    
Notes: -  

2. *I'm choosing category Images*  
Expected Result: The result should be prowly and prowly related images  
Current status: ✔️ Passed  
Notes: -  


3. *I'm clicking tools button and clicking 'black and white'*  
Expected Result: Black and white images related to prowly appears  
Current status: ✔️ Passed  
Notes: -  

4. *I'm clicking one of the images'*  
Expected Result: A zoomed-in preview of the image is shown   
Current status: ✔️ Passed  
Notes: -  


## Test case ID: TC:A-0002 
Scenario ID: 1 
Test case: News  
Test Case description: testing News category  
Test step: 


1. *I'm writing 'prowly' and clicking the submit button (magnifier icon),    
Expected Result:  News and articles with 'prowly' appears  
Current status:  ✔️ Passed  
Notes: -

2. *I'm clicking 'tools' button and choosing 'past hour'*   
Expected Result:  News and articles with 'prowly' from the past hour  
Current status:  ✔️ Passed   
Notes: From the second article has 'prowly' in it  


## Test case ID: TC:A-0003  
Scenario ID: 1   
Test case: Video 
Test Case description: testing Video category  
Test step:  


1. *Im writing 'prowly' and clicking the submit button (magnifier icon)*    
Expected Result:  News and articles related to 'prowly' appears   
Current status: ✔️ Passed   
Notes: - 

2. *I'm clicking 'Video' button*  
Expected Result:  Videos related to 'prowly' appears   
Current status:  ✔️ Passed  
Notes: - 

3. *I'm clicking one of the video*  
Expected Result: prowly website open with video attached  
Current status:  ✔️ Passed  
Notes: user need to scroll down a little bit 

Test Scenario:B (TS:B)
===============
## ID:1 Filtering by date

### Description

Check the possibility to find _prowly_ by using date filter via google.com

### Pre-Conditions

User needs to go on the [google.com](https://google.com) and click the search textarea

### Expected results

 Google should work and user should have possibility to write something in the search textarea

## Test case ID: TC:B-0001 
Scenario ID: 1   
Test case: Date  
Test case description: testing past hour filter
Test step: 

Prerequisites: after all steps user need to click the submit button (magnifier icon)

1. *I'm writing 'Prowly' and clicking the submit button (magnifier icon)*  
Expected Result: The result should be prowly and prowly related websites  
Current status: ✔️ Passed  
Notes: -  

3. *I'm clicking tools button and clicking 'Past hour'*  
Expected Result: It should show pages and documents related to prowly from the past hour  
Current status: ✔️ Passed  
Notes: There was no documents from past hour so there was nothing in results  

4. *I'm clicking one of the images'*  
Expected Result: A zoomed-in preview of the image is shown   
Current status: ✔️ Passed  
Notes: - 

 ## Test case ID: TC:B-0002 
Scenario ID: 1   
Test case: Date  
Test case description: testing past hour filter
Test step: 

Prerequisites: after all steps user need to click the submit button (magnifier icon)

1. *I'm writing 'Prowly' and clicking the submit button (magnifier icon)*  
Expected Result: The result should be prowly and prowly related websites  
Notes: -  

3. *I'm clicking tools button and clicking 'Past hour'*  
Expected Result: It should show pages and documents related to prowly from the past hour
Current status: ✔️ Passed  
Notes: There was no documents from past hour so there was nothing in results  

4. *I'm clicking one of the images'*  
Expected Result: A zoomed-in preview of the image is shown   
Current status: ✔️ Passed  
Notes: - 

 ## Test case ID: TC:B-0003 
Scenario ID: 1   
Test case: Date  
Test case description: testing custom range filter  
Test step: 

Prerequisites: after all steps user need to click the submit button (magnifier icon)

1. *I'm writing 'Prowly' and clicking the submit button (magnifier icon)*    
Expected Result: The result should be prowly and prowly related websites  
Current status: ✔️ Passed 
Notes: -  

3. *I'm clicking tools button and clicking 'custom range*    
Expected Result: Modal popup appear which allows to choose date range  
Current status: ✔️ Passed    
Notes: -     

4. *I'm writing 'pieski' in 'from' input and 'czupakabra' in 'to' input, in the end I'm clicking "go" button 
Expected Result: It should show error message    
Current status: ✖️ Failed  
Notes: There is no error message, instead of this it's setting up today date